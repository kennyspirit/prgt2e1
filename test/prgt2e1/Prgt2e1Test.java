/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author paco
 */
public class Prgt2e1Test {
  
  static  Prgt2e1 prgt2e1;
  static  double euros;

  
  
  public Prgt2e1Test() {
  }
  
  @BeforeClass
  public static void setUpClass() {
    Prgt2e1 prgt2e1 = new Prgt2e1 ();
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }

  /**
   * Test of main method, of class Prgt2e1.
   */
  @Test
  public void testMain() {
    
    int billete200;
    double billete20;
    double billete10;
    double resto;
    double res;
    
    System.out.println("main");
    String[] args = null;
    Prgt2e1.main(args);
    
    euros=Prgt2e1.euros;
    billete200=(int)euros/200;
    resto=euros%200;
    billete20=(int)resto/20;
    resto%=20;
    billete10=(int)resto/10;
    resto=(int)resto%10;
    
    System.out.println("Test ... ");
    Assert.assertEquals ( "billete200 ", billete200 , Prgt2e1.billete200 , 1e-6);
    Assert.assertEquals ( "billete20 ", billete20 , Prgt2e1.billete20 , 1e-6);
    Assert.assertEquals ( "billete10 ", billete10 , Prgt2e1.billete10 , 1e-6);
    Assert.assertEquals ( "monedas ", resto , Prgt2e1.resto , 1e-6);
    // TODO review the generated test code and remove the default call to fail.
    //fail("The test case is a prototype.");
    
  }
}